
package com.cindodcindy.masakin.pojo.payment;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoUserPostPayment {

    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("bankpengirim")
    @Expose
    private String bankpengirim;
    @SerializedName("namapengirim")
    @Expose
    private String namapengirim;
    @SerializedName("jumlahUang")
    @Expose
    private String jumlahUang;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoUserPostPayment() {
    }

    /**
     * 
     * @param createdAt
     * @param namapengirim
     * @param bankpengirim
     * @param id
     * @param jumlahUang
     * @param updatedAt
     * @param username
     */
    public PojoUserPostPayment(String createdAt, String updatedAt, long id, String username, String bankpengirim, String namapengirim, String jumlahUang) {
        super();
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.id = id;
        this.username = username;
        this.bankpengirim = bankpengirim;
        this.namapengirim = namapengirim;
        this.jumlahUang = jumlahUang;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public PojoUserPostPayment withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PojoUserPostPayment withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PojoUserPostPayment withId(long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PojoUserPostPayment withUsername(String username) {
        this.username = username;
        return this;
    }

    public String getBankpengirim() {
        return bankpengirim;
    }

    public void setBankpengirim(String bankpengirim) {
        this.bankpengirim = bankpengirim;
    }

    public PojoUserPostPayment withBankpengirim(String bankpengirim) {
        this.bankpengirim = bankpengirim;
        return this;
    }

    public String getNamapengirim() {
        return namapengirim;
    }

    public void setNamapengirim(String namapengirim) {
        this.namapengirim = namapengirim;
    }

    public PojoUserPostPayment withNamapengirim(String namapengirim) {
        this.namapengirim = namapengirim;
        return this;
    }

    public String getJumlahUang() {
        return jumlahUang;
    }

    public void setJumlahUang(String jumlahUang) {
        this.jumlahUang = jumlahUang;
    }

    public PojoUserPostPayment withJumlahUang(String jumlahUang) {
        this.jumlahUang = jumlahUang;
        return this;
    }

}
