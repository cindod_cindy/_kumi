package com.cindodcindy.masakin.shared_pref;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUserHandle {

    public static final String SP_CINDYKITCHEN_APP = "spCindyKitchenApp";

    public static final Long SP_ID = 1L;
    public static final String SP_NAMA = "spNama";
    public static final String SP_NAMA_Regis = "spNamaRegis";
    public static final String SP_PHONE = "spPhone";
    public static final String SP_EMAIL = "spEmail";

    public static final String SP_PASSWORD="spPassword";
    public static final String SP_TOKEN = "spToken";

    public static final String SP_SUDAH_LOGIN = "spSudahLogin";

    public static final String SP_SUDAH_REGIS = "spSudahRegis";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public SharedPrefUserHandle(Context context){
        sp = context.getSharedPreferences(SP_CINDYKITCHEN_APP, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void setSpId(Long keyId, Long valueId){
        spEditor.putLong(String.valueOf(keyId), valueId);
        spEditor.commit();
    }

    public void setSpNama(String keyName, String valueName){
        spEditor.putString(keyName,valueName);
        spEditor.commit();
    }

    public void setSpNamaRegis(String keyNameRegis, String valueNameRegis){
        spEditor.putString(keyNameRegis,valueNameRegis);
        spEditor.commit();
    }


    public void setSpPhone(String keyPhone, String valuePhone){
        spEditor.putString(keyPhone,valuePhone);
        spEditor.commit();
    }

    public void setSpEmail(String keyEmail, String valueEmail){
        spEditor.putString(keyEmail,valueEmail);
        spEditor.commit();
    }

    public void setSpPassword(String keyPassword, String valuePassword){
        spEditor.putString(keyPassword,valuePassword);
        spEditor.commit();
    }



    public  void  setSpToken(String keyToken, String valueToken){
        spEditor.putString(keyToken, valueToken);
        spEditor.commit();
    }


    public void saveSPBoolean(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public void saveSPBooleanRegis(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }


    public Long getSpId(){
        return  sp.getLong(String.valueOf(SP_ID), 1l);
    }

    public String  getSpNama(){
        return sp.getString(SP_NAMA, "");
    }

    public String  getSpNamaRegis(){
        return sp.getString(SP_NAMA_Regis, "");
    }


    public String getSpPhone(){
        return  sp.getString(SP_PHONE, "");
    }

    public String getSPEmail(){
        return sp.getString(SP_EMAIL, "");
    }

    public String getSpToken(){
        return  sp.getString(SP_TOKEN, "");
    }

    public Boolean getSPSudahLogin(){
        return sp.getBoolean(SP_SUDAH_LOGIN, false);
    }

    public Boolean getSPSudahRegis(){
        return sp.getBoolean(SP_SUDAH_REGIS, false);
    }
}
