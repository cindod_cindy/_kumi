
package com.cindodcindy.masakin.pojo.get_warn;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class Sort__1 {

    @SerializedName("empty")
    @Expose
    private boolean empty;
    @SerializedName("sorted")
    @Expose
    private boolean sorted;
    @SerializedName("unsorted")
    @Expose
    private boolean unsorted;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Sort__1() {
    }

    /**
     * 
     * @param unsorted
     * @param sorted
     * @param empty
     */
    public Sort__1(boolean empty, boolean sorted, boolean unsorted) {
        super();
        this.empty = empty;
        this.sorted = sorted;
        this.unsorted = unsorted;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public Sort__1 withEmpty(boolean empty) {
        this.empty = empty;
        return this;
    }

    public boolean isSorted() {
        return sorted;
    }

    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    public Sort__1 withSorted(boolean sorted) {
        this.sorted = sorted;
        return this;
    }

    public boolean isUnsorted() {
        return unsorted;
    }

    public void setUnsorted(boolean unsorted) {
        this.unsorted = unsorted;
    }

    public Sort__1 withUnsorted(boolean unsorted) {
        this.unsorted = unsorted;
        return this;
    }

}
