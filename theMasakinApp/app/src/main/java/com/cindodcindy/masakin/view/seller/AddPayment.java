package com.cindodcindy.masakin.view.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cindodcindy.masakin.PilihLoginAs;
import com.cindodcindy.masakin.R;
import com.cindodcindy.masakin.RegisUser;
import com.cindodcindy.masakin.pojo.payment.PojoUserPostPayment;
import com.cindodcindy.masakin.pojo.regis.PojoRegis;
import com.cindodcindy.masakin.retrofit.RetrofitHandleApi;
import com.cindodcindy.masakin.retrofit.RetrofitMethodInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPayment extends AppCompatActivity {

    private TextView textView_username, textView_btn_send_payment;

    private EditText editText_bank_pengirim, editText_nama_pengirim, editText_jumlah_uang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);

        textView_username=findViewById(R.id.tv_payment_username);
        editText_bank_pengirim=findViewById(R.id.et_payment_bank_pengirim);
        editText_nama_pengirim=findViewById(R.id.et_payment_nama_pengirim);
        editText_jumlah_uang=findViewById(R.id.et_payment_jumlah_uang);
        textView_btn_send_payment=findViewById(R.id.tv_btn_send_payment);

        textView_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText_bank_pengirim.getText().toString().isEmpty()&&
                        editText_nama_pengirim.getText().toString().isEmpty()&&
                        editText_jumlah_uang.getText().toString().isEmpty()
                        ){
                    editText_bank_pengirim.setError("bank belum diisi");
                    editText_nama_pengirim.setError("nama pengirim belum diisi");
                    editText_jumlah_uang.setError("jumlah belum diisi");


                }else {
                    userPostPayment();


                }
            }
        });
    }

    public void userPostPayment(){

        String username = textView_username.getText().toString();
        String bankPengirim = editText_bank_pengirim.getText().toString();
        String namaPengirim = editText_nama_pengirim.getText().toString();
        String jumlahKirim = editText_jumlah_uang.getText().toString();

        JsonObject jsonObject = new JsonObject();
//        JsonArray jsonArray = new JsonArray();
//        jsonArray.add("admin");
//        jsonArray.add("admin");

        jsonObject.addProperty("username", username);
        jsonObject.addProperty("bankpengirim", bankPengirim);
        jsonObject.addProperty("namapengirim",namaPengirim );
        jsonObject.addProperty("jumlahUang", jumlahKirim);

        RetrofitMethodInterface retrofitMethodInterface =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoUserPostPayment> call= retrofitMethodInterface.postPayment(jsonObject);
        call.enqueue(new Callback<PojoUserPostPayment>() {
            @Override
            public void onResponse(Call<PojoUserPostPayment> call, Response<PojoUserPostPayment> response) {
                if(response.isSuccessful()){
//                    PojoUserPostPayment postPayment=response.body();
//                    postPayment.getMessage();
                    Intent intent = new Intent(AddPayment.this,KonfirmVerificationCode.class);
                    startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(AddPayment.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(AddPayment.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(AddPayment.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(AddPayment.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoUserPostPayment> call, Throwable t) {
                Toast.makeText(AddPayment.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}