
package com.cindodcindy.masakin.pojo.regis;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoUserRegisTration {

    @SerializedName("message")
    @Expose
    private String message;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoUserRegisTration() {
    }

    /**
     * 
     * @param message
     */
    public PojoUserRegisTration(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PojoUserRegisTration withMessage(String message) {
        this.message = message;
        return this;
    }

}
