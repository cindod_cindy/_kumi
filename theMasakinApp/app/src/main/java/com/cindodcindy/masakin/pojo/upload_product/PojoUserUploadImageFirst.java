
package com.cindodcindy.masakin.pojo.upload_product;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoUserUploadImageFirst {

    @SerializedName("fileName")
    @Expose
    private String fileName;
    @SerializedName("fileDownloadUri")
    @Expose
    private String fileDownloadUri;
    @SerializedName("fileType")
    @Expose
    private String fileType;
    @SerializedName("size")
    @Expose
    private long size;
    @SerializedName("productname")
    @Expose
    private String productname;
    @SerializedName("productdescription")
    @Expose
    private String productdescription;
    @SerializedName("sellername")
    @Expose
    private String sellername;
    @SerializedName("productprice")
    @Expose
    private String productprice;
    @SerializedName("productlocation")
    @Expose
    private String productlocation;
    @SerializedName("productavaliable")
    @Expose
    private String productavaliable;
    @SerializedName("productcontact")
    @Expose
    private String productcontact;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoUserUploadImageFirst() {
    }

    /**
     * 
     * @param productlocation
     * @param fileName
     * @param fileDownloadUri
     * @param size
     * @param productdescription
     * @param productcontact
     * @param productname
     * @param sellername
     * @param productavaliable
     * @param productprice
     * @param fileType
     */
    public PojoUserUploadImageFirst(String fileName, String fileDownloadUri, String fileType, long size, String productname, String productdescription, String sellername, String productprice, String productlocation, String productavaliable, String productcontact) {
        super();
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
        this.productname = productname;
        this.productdescription = productdescription;
        this.sellername = sellername;
        this.productprice = productprice;
        this.productlocation = productlocation;
        this.productavaliable = productavaliable;
        this.productcontact = productcontact;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public PojoUserUploadImageFirst withFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getFileDownloadUri() {
        return fileDownloadUri;
    }

    public void setFileDownloadUri(String fileDownloadUri) {
        this.fileDownloadUri = fileDownloadUri;
    }

    public PojoUserUploadImageFirst withFileDownloadUri(String fileDownloadUri) {
        this.fileDownloadUri = fileDownloadUri;
        return this;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public PojoUserUploadImageFirst withFileType(String fileType) {
        this.fileType = fileType;
        return this;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public PojoUserUploadImageFirst withSize(long size) {
        this.size = size;
        return this;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public PojoUserUploadImageFirst withProductname(String productname) {
        this.productname = productname;
        return this;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }

    public PojoUserUploadImageFirst withProductdescription(String productdescription) {
        this.productdescription = productdescription;
        return this;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public PojoUserUploadImageFirst withSellername(String sellername) {
        this.sellername = sellername;
        return this;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public PojoUserUploadImageFirst withProductprice(String productprice) {
        this.productprice = productprice;
        return this;
    }

    public String getProductlocation() {
        return productlocation;
    }

    public void setProductlocation(String productlocation) {
        this.productlocation = productlocation;
    }

    public PojoUserUploadImageFirst withProductlocation(String productlocation) {
        this.productlocation = productlocation;
        return this;
    }

    public String getProductavaliable() {
        return productavaliable;
    }

    public void setProductavaliable(String productavaliable) {
        this.productavaliable = productavaliable;
    }

    public PojoUserUploadImageFirst withProductavaliable(String productavaliable) {
        this.productavaliable = productavaliable;
        return this;
    }

    public String getProductcontact() {
        return productcontact;
    }

    public void setProductcontact(String productcontact) {
        this.productcontact = productcontact;
    }

    public PojoUserUploadImageFirst withProductcontact(String productcontact) {
        this.productcontact = productcontact;
        return this;
    }

}
