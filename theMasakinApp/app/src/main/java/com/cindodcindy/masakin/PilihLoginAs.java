package com.cindodcindy.masakin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cindodcindy.masakin.pojo.login.PojoUserLoin;
import com.cindodcindy.masakin.retrofit.RetrofitHandleApi;
import com.cindodcindy.masakin.retrofit.RetrofitMethodInterface;
import com.cindodcindy.masakin.shared_pref.SharedPrefUserHandle;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PilihLoginAs extends AppCompatActivity {

    private EditText editText_nama, editText_password;

    private TextView  textView_btn_login_seller;
    private TextView textView_lupa_password;

    private SharedPrefUserHandle sharedPrefUserHandle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_login_as);

        editText_nama=findViewById(R.id.et_user_login_name_seller);
        editText_password=findViewById(R.id.et_user_login_password_seller);

        textView_btn_login_seller=findViewById(R.id.tv_user_login_btn_seller);
        textView_lupa_password=findViewById(R.id.tv_login_lupa_password);

        textView_lupa_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PilihLoginAs.this,RegisUser.class);
                startActivity(intent);
                finish();
            }
        });

        sharedPrefUserHandle = new SharedPrefUserHandle(PilihLoginAs.this);
        if (sharedPrefUserHandle.getSPSudahLogin()){
            startActivity(new Intent(PilihLoginAs.this, FoodListMitra.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }


        textView_btn_login_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText_nama.getText().toString().isEmpty() && editText_password.getText().toString().isEmpty()){
                    editText_nama.setError("nama kamu belum diisi");
                    editText_password.setError("password kamu belum diisi");


                }else {
                    loginSeller();
                }

            }
        });
    }


    public void loginSeller(){

        String username = editText_nama.getText().toString();
        String password = editText_password.getText().toString();

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("username", username);
        jsonObject.addProperty("password", password);

        RetrofitMethodInterface retrofitMethodInterface =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoUserLoin> call= retrofitMethodInterface.isLoginValid(jsonObject);
        call.enqueue(new Callback<PojoUserLoin>() {
            @Override
            public void onResponse(Call<PojoUserLoin> call, Response<PojoUserLoin> response) {
                if(response.isSuccessful()){

                    PojoUserLoin pojoLogin= response.body();

                    sharedPrefUserHandle.setSpNama(SharedPrefUserHandle.SP_NAMA,editText_nama.getText().toString());
                    sharedPrefUserHandle.setSpPassword(SharedPrefUserHandle.SP_PASSWORD,editText_password.getText().toString());
                    sharedPrefUserHandle.setSpPhone(SharedPrefUserHandle.SP_PHONE,pojoLogin.getPhone());
                    sharedPrefUserHandle.setSpEmail(SharedPrefUserHandle.SP_EMAIL,pojoLogin.getEmail());
                    sharedPrefUserHandle.setSpToken(SharedPrefUserHandle.SP_TOKEN,pojoLogin.getAccessToken());
                    sharedPrefUserHandle.setSpId(SharedPrefUserHandle.SP_ID,pojoLogin.getId());
                    sharedPrefUserHandle.saveSPBoolean(SharedPrefUserHandle.SP_SUDAH_LOGIN, true);
                    startActivity(new Intent(PilihLoginAs.this, FoodListMitra.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(PilihLoginAs.this, response.message(), Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(PilihLoginAs.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(PilihLoginAs.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(PilihLoginAs.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoUserLoin> call, Throwable t) {
                Toast.makeText(PilihLoginAs.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }


}
