package com.cindodcindy.masakin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.cindodcindy.masakin.pojo.get_weekend.Content;
import com.cindodcindy.masakin.retrofit.RetrofitHandleApi;
import com.cindodcindy.masakin.retrofit.RetrofitMethodInterface;
import com.cindodcindy.masakin.view.seller.AddFoodList;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoodListMitra extends AppCompatActivity {

    private FloatingActionButton floatingActionButton_add_food;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list_mitra);
        floatingActionButton_add_food=findViewById(R.id.fab1_add_product);

        floatingActionButton_add_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FoodListMitra.this, AddFoodList.class);
                startActivity(intent);
            }
        });
    }

    public void getWeekend(){

        // Long id=1L;

        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<Content> weekendCall= methodsFactory.getWekeend();
        weekendCall.enqueue(new Callback<Content>() {
            @Override
            public void onResponse(Call<com.cindodcindy.masakin.pojo.get_weekend.Content> call, Response<Content> response) {
                if(response.isSuccessful()){
//                    com.cindodcindy.masakin.pojo.get_warn.Content content = response.body();
//                    content.setUsername(textView_get_username.getText().toString());
//                    content.setWarnVariable(textView_get_warn.getText().toString());
//                    PojoUserPostDone pojoUserPostDone=response.body();
//                    pojoRegis.getMessage();
                    //Intent intent = new Intent(KonfirmVerificationCode.this,AddFoodList.class);
                    //startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(FoodListMitra.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(FoodListMitra.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(FoodListMitra.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(FoodListMitra.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<com.cindodcindy.masakin.pojo.get_weekend.Content> call, Throwable t) {
                Toast.makeText(FoodListMitra.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}