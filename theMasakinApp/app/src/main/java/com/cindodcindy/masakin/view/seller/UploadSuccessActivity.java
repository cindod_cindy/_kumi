package com.cindodcindy.masakin.view.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.cindodcindy.masakin.R;

public class UploadSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_success);
    }
}