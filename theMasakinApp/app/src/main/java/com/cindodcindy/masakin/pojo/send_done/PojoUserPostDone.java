
package com.cindodcindy.masakin.pojo.send_done;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoUserPostDone {

    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("verification_number")
    @Expose
    private String verificationNumber;
    @SerializedName("username")
    @Expose
    private String username;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoUserPostDone() {
    }

    /**
     * 
     * @param createdAt
     * @param verificationNumber
     * @param id
     * @param updatedAt
     * @param username
     */
    public PojoUserPostDone(String createdAt, String updatedAt, long id, String verificationNumber, String username) {
        super();
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.id = id;
        this.verificationNumber = verificationNumber;
        this.username = username;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public PojoUserPostDone withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PojoUserPostDone withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PojoUserPostDone withId(long id) {
        this.id = id;
        return this;
    }

    public String getVerificationNumber() {
        return verificationNumber;
    }

    public void setVerificationNumber(String verificationNumber) {
        this.verificationNumber = verificationNumber;
    }

    public PojoUserPostDone withVerificationNumber(String verificationNumber) {
        this.verificationNumber = verificationNumber;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PojoUserPostDone withUsername(String username) {
        this.username = username;
        return this;
    }

}
