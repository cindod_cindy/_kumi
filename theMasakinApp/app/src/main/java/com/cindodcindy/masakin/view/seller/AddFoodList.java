package com.cindodcindy.masakin.view.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cindodcindy.masakin.R;
import com.cindodcindy.masakin.pojo.upload_product.PojoUserUploadImageFirst;
import com.cindodcindy.masakin.retrofit.RetrofitHandleApi;
import com.cindodcindy.masakin.retrofit.RetrofitMethodInterface;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddFoodList extends AppCompatActivity {


    private ImageView imageView_example;
    private EditText editText_prod_name, editText_prod_desc, editText_seller_name,
    editText_prod_price, editText_prod_location, editText_prod_availiable, editText_prod_contact;
    private TextView textView_btn_send_image;

    Button btnUpload, btnMulUpload, btnPickImage, btnPickVideo;
    String mediaPath, mediaPath1;
    ImageView imgView;
    String[] mediaColumns = {MediaStore.Video.Media._ID};
    ProgressDialog progressDialog;
    TextView str1, str2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food_list);
        imageView_example=findViewById(R.id.iv_add_food);
        editText_prod_name=findViewById(R.id.et_add_food_food_name);
        editText_prod_desc=findViewById(R.id.et_add_food_deskription);
        editText_seller_name=findViewById(R.id.et_add_food_food_seller_name);
        editText_prod_price=findViewById(R.id.et_add_food_food_price);
        editText_prod_location=findViewById(R.id.et_add_food_food_location);
        editText_prod_availiable=findViewById(R.id.et_add_food_food_avaliable);
        editText_prod_contact=findViewById(R.id.et_add_food_food_phone);
        textView_btn_send_image=findViewById(R.id.tv_btn_add_food);

        textView_btn_send_image.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 0);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && null != data) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                str1.setText(mediaPath);
                // Set the Image in ImageView for Previewing the Media
                imgView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                cursor.close();

            } // When an Video is picked
            else {
                Toast.makeText(this, "You haven't picked Image/Video", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }


    // Uploading Image/Video
    private void uploadFile() {
        progressDialog.show();


                String prodName= editText_prod_name.getText().toString();
                String prodDesc=editText_prod_desc.getText().toString();
                String sell_name=editText_seller_name.getText().toString();
                String prod_price=editText_prod_price.getText().toString();
                String prodLocation=editText_prod_location.getText().toString();
                String prod_avaliable=editText_prod_availiable.getText().toString();
                String prod_contact= editText_prod_contact.getText().toString();



        // Map is used to multipart the file using okhttp3.RequestBody
        File file = new File(mediaPath);

        // Parsing any Media type file

        RequestBody requestBody = RequestBody.create(file, MediaType.parse("image/*"));
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody filename = RequestBody.create(file.getName(), MediaType.parse("text/plain"));
                //RequestBody.create(MediaType.parse("text/plain"), file.getName());

        RetrofitMethodInterface getResponse = RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoUserUploadImageFirst> call = getResponse.uploadImage(fileToUpload, filename,prodName,
                prodDesc,sell_name,prod_price,prodLocation,prod_avaliable,prod_contact
                );
        call.enqueue(new Callback<PojoUserUploadImageFirst>() {
            @Override
            public void onResponse(Call<PojoUserUploadImageFirst> call, Response<PojoUserUploadImageFirst> response) {
                if(response.isSuccessful()){
//                    PojoUserPostPayment postPayment=response.body();
//                    postPayment.getMessage();
                    Intent intent = new Intent(AddFoodList.this,KonfirmVerificationCode.class);
                    startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(AddFoodList.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(AddFoodList.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(AddFoodList.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(AddFoodList.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoUserUploadImageFirst> call, Throwable t) {
                Toast.makeText(AddFoodList.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });
    }

}