package com.cindodcindy.masakin.view.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.cindodcindy.masakin.R;
import com.cindodcindy.masakin.pojo.get_verif_code.Content;
import com.cindodcindy.masakin.pojo.get_warn.PojoUserGetWarn;
import com.cindodcindy.masakin.pojo.send_done.PojoUserPostDone;
import com.cindodcindy.masakin.retrofit.RetrofitHandleApi;
import com.cindodcindy.masakin.retrofit.RetrofitMethodInterface;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WarnLayout extends AppCompatActivity {
    private TextView textView_get_username, textView_get_warn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warn_layout);
        textView_get_username=findViewById(R.id.tv_warn_get_myname);
        textView_get_warn=findViewById(R.id.tv_get_warn_user);

        getWarn();
    }


    public void getWarn(){

       Long id=1L;

        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<com.cindodcindy.masakin.pojo.get_warn.Content> warnCall= methodsFactory.getWarn(id);
        warnCall.enqueue(new Callback<com.cindodcindy.masakin.pojo.get_warn.Content>() {
            @Override
            public void onResponse(Call<com.cindodcindy.masakin.pojo.get_warn.Content> call, Response<com.cindodcindy.masakin.pojo.get_warn.Content> response) {
                if(response.isSuccessful()){
                    com.cindodcindy.masakin.pojo.get_warn.Content content = response.body();
                    content.setUsername(textView_get_username.getText().toString());
                    content.setWarnVariable(textView_get_warn.getText().toString());
//                    PojoUserPostDone pojoUserPostDone=response.body();
//                    pojoRegis.getMessage();
                    //Intent intent = new Intent(KonfirmVerificationCode.this,AddFoodList.class);
                    //startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(WarnLayout.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(WarnLayout.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(WarnLayout.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(WarnLayout.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<com.cindodcindy.masakin.pojo.get_warn.Content> call, Throwable t) {
                Toast.makeText(WarnLayout.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}