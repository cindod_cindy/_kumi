package com.cindodcindy.masakin.view.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cindodcindy.masakin.PilihLoginAs;
import com.cindodcindy.masakin.R;
import com.cindodcindy.masakin.RegisUser;
import com.cindodcindy.masakin.pojo.get_verif_code.Content;
import com.cindodcindy.masakin.pojo.regis.PojoRegis;
import com.cindodcindy.masakin.pojo.send_done.PojoUserPostDone;
import com.cindodcindy.masakin.retrofit.RetrofitHandleApi;
import com.cindodcindy.masakin.retrofit.RetrofitMethodInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KonfirmVerificationCode extends AppCompatActivity {

    private TextView textView_usrname,  textView_btn_send_to_done;
    private TextView textView_getVerification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirm_verification_code);
        textView_usrname=findViewById(R.id.tv_user_get_verif_name);
        textView_getVerification=findViewById(R.id.tv_user_get_verif_code);
        textView_btn_send_to_done=findViewById(R.id.tv_user_get_verif_btn_send_to_done);

        textView_btn_send_to_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textView_usrname.getText().toString().isEmpty()&&textView_getVerification.getText().toString().isEmpty()){
                    textView_usrname.setError("nama kosong");
                    textView_getVerification.setError("kode verifikasi kosong");


                }else {
                    sendDone();


                }


            }
        });

        getVerifCode();
    }

    public void getVerifCode(){

        String name = textView_usrname.getText().toString();
        String verifCode = textView_getVerification.getText().toString();
//        String email = editText_email.getText().toString();
//        String password = editText_password.getText().toString();
//
//        JsonObject jsonObject = new JsonObject();
//        JsonArray jsonArray = new JsonArray();
//        jsonArray.add("admin");
//        jsonArray.add("admin");
//
//        jsonObject.addProperty("name", name);
//        jsonObject.addProperty("phone", phone);
//        jsonObject.addProperty("email",email );
//        jsonObject.addProperty("password", password);
//        jsonObject.add("role", jsonArray);

        Long idPayment=1L;

        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<Content> callContent= methodsFactory.getVerifCodeFromAdmin(idPayment);
        callContent.enqueue(new Callback<Content>() {
            @Override
            public void onResponse(Call<Content> call, Response<Content> response) {
                if(response.isSuccessful()){
                    Content content=response.body();
                    content.setVerificationCode(textView_getVerification.getText().toString());
                    content.setUsername(textView_usrname.getText().toString());
//                    Intent intent = new Intent(RegisUser.this, PilihLoginAs.class);
//                    startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(KonfirmVerificationCode.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(KonfirmVerificationCode.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(KonfirmVerificationCode.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(KonfirmVerificationCode.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<Content> call, Throwable t) {
                Toast.makeText(KonfirmVerificationCode.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void sendDone(){

        String name = textView_usrname.getText().toString();
        String verifCode = textView_getVerification.getText().toString();

        Long id=1L;

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("verification_number", verifCode);
        jsonObject.addProperty("username", name);


        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoUserPostDone> postDoneCall= methodsFactory.postDoneUser(id,jsonObject);
        postDoneCall.enqueue(new Callback<PojoUserPostDone>() {
            @Override
            public void onResponse(Call<PojoUserPostDone> call, Response<PojoUserPostDone> response) {
                if(response.isSuccessful()){
//                    PojoUserPostDone pojoUserPostDone=response.body();
//                    pojoRegis.getMessage();
                    Intent intent = new Intent(KonfirmVerificationCode.this,AddFoodList.class);
                    startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(KonfirmVerificationCode.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(KonfirmVerificationCode.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(KonfirmVerificationCode.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(KonfirmVerificationCode.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoUserPostDone> call, Throwable t) {
                Toast.makeText(KonfirmVerificationCode.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}