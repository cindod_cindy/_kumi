package com.cindodcindy.masakin.retrofit;

import com.cindodcindy.masakin.pojo.get_verif_code.Content;
import com.cindodcindy.masakin.pojo.login.PojoLogin;
import com.cindodcindy.masakin.pojo.login.PojoUserLoin;
import com.cindodcindy.masakin.pojo.payment.PojoUserPostPayment;
import com.cindodcindy.masakin.pojo.regis.PojoRegis;
import com.cindodcindy.masakin.pojo.regis.PojoUserRegisTration;
import com.cindodcindy.masakin.pojo.send_done.PojoUserPostDone;
import com.cindodcindy.masakin.pojo.upload_product.PojoUserUploadImageFirst;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface RetrofitMethodInterface {
    @Headers({
            "Content-Type:application/json"
    })
    @POST("auth/signup")
    Call<PojoUserRegisTration> isRegistration(@Body JsonObject body);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("auth/signin")
    Call<PojoUserLoin> isLoginValid(@Body JsonObject body);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("payment/postpayment")
    Call<PojoUserPostPayment> postPayment(@Body JsonObject body);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("verification/payment/{paymentId}/getverifications")
    Call<Content> getVerifCodeFromAdmin(@Path("id") Long id);


    @Headers({
            "Content-Type:application/json"
    })
    @POST("donepayment/verification/{verificationId}/postdone")
    Call<PojoUserPostDone> postDoneUser(@Path ("verificationId") Long id,@Body JsonObject body);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("warnpayment/done/{doneId}/postwarn")
    Call<com.cindodcindy.masakin.pojo.get_warn.Content> getWarn(@Path ("doneId") Long id);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("week/getweek")
    Call<com.cindodcindy.masakin.pojo.get_weekend.Content> getWekeend();

    @Multipart
    @POST("/images/upload")
    Call<PojoUserUploadImageFirst> uploadImage(@Part MultipartBody.Part image,
                                               @Part("file") RequestBody name,
                                               @Part("productname") String foodName,
                                               @Part("productdescription") String productdescription,
                                               @Part("sellername") String sellername,
                                               @Part("productprice") String productprice,
                                               @Part("productlocation") String productlocation,
                                               @Part("productavaliable") String productavaliable,
                                               @Part("productcontact") String productcontact
                                               );








}
