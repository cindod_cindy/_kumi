
package com.cindodcindy.masakin.pojo.get_warn;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoUserGetWarn {

    @SerializedName("content")
    @Expose
    private List<Content> content = new ArrayList<Content>();
    @SerializedName("pageable")
    @Expose
    private Pageable pageable;
    @SerializedName("last")
    @Expose
    private boolean last;
    @SerializedName("totalElements")
    @Expose
    private long totalElements;
    @SerializedName("totalPages")
    @Expose
    private long totalPages;
    @SerializedName("number")
    @Expose
    private long number;
    @SerializedName("size")
    @Expose
    private long size;
    @SerializedName("sort")
    @Expose
    private Sort__1 sort;
    @SerializedName("first")
    @Expose
    private boolean first;
    @SerializedName("numberOfElements")
    @Expose
    private long numberOfElements;
    @SerializedName("empty")
    @Expose
    private boolean empty;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoUserGetWarn() {
    }

    /**
     * 
     * @param number
     * @param last
     * @param size
     * @param numberOfElements
     * @param totalPages
     * @param pageable
     * @param sort
     * @param content
     * @param first
     * @param totalElements
     * @param empty
     */
    public PojoUserGetWarn(List<Content> content, Pageable pageable, boolean last, long totalElements, long totalPages, long number, long size, Sort__1 sort, boolean first, long numberOfElements, boolean empty) {
        super();
        this.content = content;
        this.pageable = pageable;
        this.last = last;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.number = number;
        this.size = size;
        this.sort = sort;
        this.first = first;
        this.numberOfElements = numberOfElements;
        this.empty = empty;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public PojoUserGetWarn withContent(List<Content> content) {
        this.content = content;
        return this;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

    public PojoUserGetWarn withPageable(Pageable pageable) {
        this.pageable = pageable;
        return this;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public PojoUserGetWarn withLast(boolean last) {
        this.last = last;
        return this;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public PojoUserGetWarn withTotalElements(long totalElements) {
        this.totalElements = totalElements;
        return this;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public PojoUserGetWarn withTotalPages(long totalPages) {
        this.totalPages = totalPages;
        return this;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public PojoUserGetWarn withNumber(long number) {
        this.number = number;
        return this;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public PojoUserGetWarn withSize(long size) {
        this.size = size;
        return this;
    }

    public Sort__1 getSort() {
        return sort;
    }

    public void setSort(Sort__1 sort) {
        this.sort = sort;
    }

    public PojoUserGetWarn withSort(Sort__1 sort) {
        this.sort = sort;
        return this;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public PojoUserGetWarn withFirst(boolean first) {
        this.first = first;
        return this;
    }

    public long getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(long numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public PojoUserGetWarn withNumberOfElements(long numberOfElements) {
        this.numberOfElements = numberOfElements;
        return this;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public PojoUserGetWarn withEmpty(boolean empty) {
        this.empty = empty;
        return this;
    }

}
