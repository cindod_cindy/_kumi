
package com.cindodcindy.masakin.pojo.regis;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoRegis {

    @SerializedName("message")
    @Expose
    private String message;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoRegis() {
    }

    /**
     * 
     * @param message
     */
    public PojoRegis(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PojoRegis withMessage(String message) {
        this.message = message;
        return this;
    }

}
