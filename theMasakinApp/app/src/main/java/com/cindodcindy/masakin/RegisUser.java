package com.cindodcindy.masakin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cindodcindy.masakin.pojo.regis.PojoUserRegisTration;
import com.cindodcindy.masakin.retrofit.RetrofitHandleApi;
import com.cindodcindy.masakin.retrofit.RetrofitMethodInterface;
import com.cindodcindy.masakin.shared_pref.SharedPrefUserHandle;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisUser extends AppCompatActivity {

    private EditText editText_nama, editText_phone, editText_email, editText_password;

    private TextView textView_btn_regis;
    private TextView textView_sudah_punya_akun;

    private SharedPrefUserHandle sharedPrefUserHandle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_user);

        editText_nama=findViewById(R.id.et_user_regis_name);
        editText_phone=findViewById(R.id.et_user_regis_phone);
        editText_email=findViewById(R.id.et_user_regis_email);
        editText_password=findViewById(R.id.et_user_password);
        textView_btn_regis=findViewById(R.id.user_regis_btn);
        textView_sudah_punya_akun=findViewById(R.id.regis_sudah_punya_akun);

        textView_sudah_punya_akun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(RegisUser.this,PilihLoginAs.class);
                startActivity(intent);
                finish();
            }
        });

        sharedPrefUserHandle = new SharedPrefUserHandle(RegisUser.this);

        if (sharedPrefUserHandle.getSPSudahLogin()){
            startActivity(new Intent(RegisUser.this, PilihLoginAs.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        textView_btn_regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editText_nama.getText().toString().isEmpty()&&editText_email.getText().toString().isEmpty()&& editText_phone.getText().toString().isEmpty()&&editText_password.getText().toString().isEmpty()){
                    editText_nama.setError("nama belum diisi");
                    editText_phone.setError("nomor telepon belum diisi");
                    editText_email.setError("email belum diisi");
                    editText_password.setError("password belum diisi");


                }else {
                    userRegis();


                }

            }
        });
    }


    public void userRegis(){

        String name = editText_nama.getText().toString();
        String phone = editText_phone.getText().toString();
        String email = editText_email.getText().toString();
        String password = editText_password.getText().toString();

        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        jsonArray.add("buyer");
        jsonArray.add("seller");

        jsonObject.addProperty("username", name);
        jsonObject.addProperty("phone", phone);
        jsonObject.addProperty("email",email );
        jsonObject.addProperty("password", password);
        jsonObject.add("role", jsonArray);

        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoUserRegisTration> call= methodsFactory.isRegistration(jsonObject);
        call.enqueue(new Callback<PojoUserRegisTration>() {
            @Override
            public void onResponse(Call<PojoUserRegisTration> call, Response<PojoUserRegisTration> response) {
                if(response.isSuccessful()){
                    PojoUserRegisTration pojoRegis=response.body();
                    pojoRegis.getMessage();
                    Toast.makeText(RegisUser.this, pojoRegis.getMessage(), Toast.LENGTH_SHORT).show();
                    sharedPrefUserHandle.setSpNamaRegis(SharedPrefUserHandle.SP_NAMA_Regis,pojoRegis.getMessage());
                    sharedPrefUserHandle.saveSPBooleanRegis(SharedPrefUserHandle.SP_SUDAH_REGIS, true);
                    startActivity(new Intent(RegisUser.this, PilihLoginAs.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
//                    Intent intent = new Intent(RegisUser.this, PilihLogin.class);
//                    startActivity(intent);
//                    finish();

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(RegisUser.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(RegisUser.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(RegisUser.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(RegisUser.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoUserRegisTration> call, Throwable t) {
                Toast.makeText(RegisUser.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}